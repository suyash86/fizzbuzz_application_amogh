﻿namespace FizzBuzzApplication.Web.Tests.ControllerTests
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using FizzBuzzApplication.Service.Interface;
    using FizzBuzzApplication.Web.Controllers;
    using FizzBuzzApplication.Web.Models;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class FizzBuzzControllerTests
    {
        private FizzBuzzController controller;
        private FizzBuzzModel fizzbuzz;
        private Mock<IFizzBuzzService> mockfizzbuzzservice;
        private List<string> testlist;

        [SetUp]
        public void Initialize()
        {
            this.mockfizzbuzzservice = new Mock<IFizzBuzzService>();
            this.controller = new FizzBuzzController(this.mockfizzbuzzservice.Object);
            this.fizzbuzz = new FizzBuzzModel() { Input = 5 };
        }

        [Test]
        public void DisplayMethod_Test()
        {
            // act 
            var result = controller.Display() as ViewResult;

            // assert
            Assert.AreEqual("Display", result.ViewName);
        }

        [Test]
        public void DisplayListMethod_Test_ForFizzBuzz()
        {
            // arrange
            testlist = new List<string>() { "1", "2", "fizz", "4", "buzz" };
            mockfizzbuzzservice.Setup(m => m.GetFizzBuzzList(It.IsAny<int>())).Returns(testlist);

            // act
            ViewResult result = controller.DisplayList(fizzbuzz) as ViewResult;
            var model = result.Model as FizzBuzzModel;

            // assert
            Assert.AreEqual(fizzbuzz, model);
            Assert.AreEqual("Display", result.ViewName);
            Assert.AreEqual(testlist, model.FizzBuzzList);
        }

        [Test]
        public void DisplayListMethod_Test_ForWizzWuzz()
        {
            // arrange
            testlist = new List<string>() { "1", "2", "wizz", "4", "wuzz" };
            mockfizzbuzzservice.Setup(m => m.GetFizzBuzzList(It.IsAny<int>())).Returns(testlist);

            // act
            ViewResult result = controller.DisplayList(fizzbuzz) as ViewResult;
            var resultmodel = result.Model as FizzBuzzModel;

            // assert
            Assert.AreEqual(fizzbuzz, resultmodel);
            Assert.AreEqual("Display", result.ViewName);
            Assert.AreEqual(testlist, resultmodel.FizzBuzzList);
        }

        [Test]
        public void DisplayFizzBuzzList_Test_With_Model_Error()
        {
            // arrange
            var testfizzbuzz = new FizzBuzzModel() { Input = -2 };
            controller.ModelState.AddModelError("Input", "Enter an integer value between 1 and 1000");

            // act
            var result = controller.DisplayList(testfizzbuzz) as ViewResult;
            var resulmodel = result.Model as FizzBuzzModel;

            // assert
            Assert.AreEqual("Display", result.ViewName);
            Assert.IsNull(resulmodel.FizzBuzzList);
        }

        [Test]
        public void GetPagedList_Test()
        {
            // arrange
            int page = 1;
            var testfizzbuzz = new FizzBuzzModel()
            {
                Input = 15,
            };
            testlist = new List<string>() { "1", "2", "fizz", "4", "buzz" };
            mockfizzbuzzservice.Setup(p => p.GetFizzBuzzList(It.IsAny<int>())).Returns(testlist);

            // act
            var result = controller.GetPagedList(testfizzbuzz, page) as ViewResult;

            // Assert
            var fizzBuzz = result.Model as FizzBuzzModel;
            Assert.AreEqual(result.ViewName, "Display");
            CollectionAssert.AreEquivalent(fizzBuzz.FizzBuzzList, testlist);
        }
    }
}