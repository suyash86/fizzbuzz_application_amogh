﻿namespace FizzBuzzApplication.Web.Tests.HtmlHelper
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using FizzBuzzApplication.Web.Helper;
    using FizzBuzzApplication.Web.Tests.HelperTests;
    using NUnit.Framework;

    [TestFixture]
    public class PrintFizzBuzzListTests
    {
        private string teststring;
        private IEnumerable<string> testlist;
        private HtmlHelperMock htmlhelpermock;

        [SetUp]
        public void Initialise()
        {
            this.htmlhelpermock = new HtmlHelperMock();
        }

        [Test]
        public void PrintListMethod_Test_ForFizzBuzz()
        {
            // arrange
            this.teststring = "<div><div><Span class=\"1\">1 </Span></div><div><Span class=\"2\">2 </Span></div><div><Span class=\"fizz\">fizz </Span></div></div>";
            this.testlist = new List<string>() { "1", "2", "fizz" };

            // act
            var result = PrintFizzBuzzList.PrintList(this.htmlhelpermock.CreateHtmlHelper(new ViewDataDictionary("Test")), this.testlist);

            // assert
            Assert.IsNotEmpty(result.ToString());
            Assert.AreEqual(this.teststring, result.ToString());
        }

        [Test]
        public void PrintListMethod_Test_ForWizzWuzz()
        {
            // arrange
            this.teststring = "<div><div><Span class=\"1\">1 </Span></div><div><Span class=\"2\">2 </Span></div><div><Span class=\"wizz\">wizz </Span></div></div>";
            this.testlist = new List<string>() { "1", "2", "wizz" };

            // act
            var result = PrintFizzBuzzList.PrintList(this.htmlhelpermock.CreateHtmlHelper(new ViewDataDictionary("Test")), this.testlist);

            // assert
            Assert.IsNotEmpty(result.ToString());
            Assert.AreEqual(this.teststring, result.ToString());
        }
    }
}
