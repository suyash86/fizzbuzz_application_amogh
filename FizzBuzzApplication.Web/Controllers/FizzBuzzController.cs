﻿namespace FizzBuzzApplication.Web.Controllers
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using FizzBuzzApplication.Service.Interface;
    using FizzBuzzApplication.Web.Helper;
    using FizzBuzzApplication.Web.Models;
    using PagedList;

    public class FizzBuzzController : Controller
    {
        private readonly IFizzBuzzService fizzbuzzservice;

        public FizzBuzzController(IFizzBuzzService fizzBuzzService)
        {
            this.fizzbuzzservice = fizzBuzzService;
        }

        public ActionResult Display()
        {
            return this.View("Display");
        }

        [HttpPost]
        public ActionResult DisplayList(FizzBuzzModel fizzbuzz)
        {
            if (ModelState.IsValid)
            {
                fizzbuzz.FizzBuzzList = (IPagedList<string>)this.GetFizzBuzzList(fizzbuzz.Input, 1);
            }

            return View("Display", fizzbuzz);
        }

        [HttpGet]
        public ActionResult GetPagedList(FizzBuzzModel fizzbuzz, int page)
        {
            var model = new FizzBuzzModel() { Input = fizzbuzz.Input };
            model.FizzBuzzList = (IPagedList<string>)this.GetFizzBuzzList(fizzbuzz.Input, page);
            return this.View("Display", model);
        }

        private IEnumerable<string> GetFizzBuzzList(int number, int pageNumber)
        {
            var resultList = this.fizzbuzzservice.GetFizzBuzzList(number).ToPagedList(pageNumber, FizzBuzzWebConstants.PageSize);
            return resultList;
        }
    }
}