﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "Not Required")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "Not Required", Scope = "type", Target = "~T:FizzBuzzApplication.Web.Controllers.FizzBuzztController")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1101:Prefix local calls with this", Justification = "Not Required", Scope = "member", Target = "~M:FizzBuzzApplication.Web.Controllers.FizzBuzztController.Display~System.Web.Mvc.ActionResult")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "Not required", Scope = "member", Target = "~P:FizzBuzzApplication.Web.Models.FizzBuzzModel.Input")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "Not Required", Scope = "member", Target = "~P:FizzBuzzApplication.Web.Models.FizzBuzzModel.FizzBuzzList")]