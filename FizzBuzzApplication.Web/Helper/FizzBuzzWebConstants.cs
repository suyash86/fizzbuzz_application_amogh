﻿namespace FizzBuzzApplication.Web.Helper
{
    public static class FizzBuzzWebConstants
    {
        public const string DisplayMessage = "Enter a Number";
        public const string RequiredErrorMessage = "Enter a Positive Number";
        public const string RangeErrorMessage = "Enter number between 1 and 1000";
        public const int LowerLimit = 1;
        public const int UpperLimit = 1000;
        public const int PageSize = 20;
    }
}