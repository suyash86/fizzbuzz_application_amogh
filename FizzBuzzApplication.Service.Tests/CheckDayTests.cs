﻿namespace FizzBuzzApplication.Service.Tests
{
    using System;
    using FizzBuzzApplication.Service.Interface;
    using FizzBuzzApplication.Service.Services;
    using NUnit.Framework;

    [TestFixture]
    internal class CheckDayTests
    {
        private ICheckDay checkday;

        [SetUp]
        public void Initialise()
        {
            checkday = new CheckDay("Wednesday");
        }

        [TestCase("Monday", false)]
        [TestCase("Wednesday", true)]
        public void SpecifiedDayCheck_Test(DayOfWeek today, bool expected)
        {
            // act
            var result = checkday.SpecifiedDayChecK(today);

            // assert
            Assert.AreEqual(expected, result);
        }
    }
}