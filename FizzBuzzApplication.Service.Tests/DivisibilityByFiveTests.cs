﻿namespace FizzBuzzApplication.Service.Tests
{
    using System;
    using FizzBuzzApplication.Service.Interface;
    using FizzBuzzApplication.Service.Services;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class DivisibilityByFiveTests
    {
        private IDivisionByNumber divisiblebyfive;
        private Mock<ICheckDay> mockcheckday;

        [SetUp]
        public void Initialise()
        {
            mockcheckday = new Mock<ICheckDay>();
            divisiblebyfive = new DivisibilityByFive(mockcheckday.Object);
        }

        [TestCase(5, true)]
        [TestCase(7, false)]
        [TestCase(15, true)]
        public void IsDivisibleMethod_Test(int input, bool expected)
        {
            // act
            var result = divisiblebyfive.IsDivisible(input);

            // assert
            Assert.AreEqual(expected, result);
        }

        [TestCase(5, "buzz")]
        [TestCase(7, null)]
        [TestCase(15, "buzz")]
        public void GetMessageMethod_Test_For_Buzz(int input, string expected)
        {
            // arrange
            string result = null;

            // act
            if (divisiblebyfive.IsDivisible(input))
            {
                result = divisiblebyfive.GetMessage();
            }

            // assert
            Assert.AreEqual(expected, result);
        }

        [TestCase(5, "wuzz")]
        [TestCase(7, null)]
        [TestCase(15, "wuzz")]
        public void GetMessage_Test_For_Wuzz(int input, string expected)
        {
            // arrange
            string result = null;
            this.mockcheckday.Setup(m => m.SpecifiedDayChecK(It.IsAny<DayOfWeek>())).Returns(true);

            // act
            if (divisiblebyfive.IsDivisible(input))
            {
                result = divisiblebyfive.GetMessage();
            }

            // assert
            Assert.AreEqual(expected, result);
        }
    }
}