﻿namespace FizzBuzzApplication.Service.Tests
{
    using System;
    using FizzBuzzApplication.Service.Interface;
    using FizzBuzzApplication.Service.Services;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class DivisibilityByThreeTests
    {
        private IDivisionByNumber divisiblebythree;
        private Mock<ICheckDay> mockcheckday;

        [SetUp]
        public void Initialise()
        {
            mockcheckday = new Mock<ICheckDay>();
            divisiblebythree = new DivisibilityByThree(mockcheckday.Object);
        }

        [TestCase(3, true)]
        [TestCase(5, false)]
        [TestCase(15, true)]
        public void IsDivisibleMethod_Test(int input, bool expected)
        {
            // act
            var result = divisiblebythree.IsDivisible(input);

            // assert
            Assert.AreEqual(expected, result);
        }

        [TestCase(3, "fizz")]
        [TestCase(7, null)]
        [TestCase(15, "fizz")]
        public void GetMessageMethod_Test_For_Fizz(int input, string expected)
        {
            // arrange
            string result = null;

            // act
            if (divisiblebythree.IsDivisible(input))
            {
                result = divisiblebythree.GetMessage();
            }

            // assert
            Assert.AreEqual(expected, result);
        }

        [TestCase(3, "wizz")]
        [TestCase(7, null)]
        [TestCase(15, "wizz")]
        public void GetMessage_Test_For_Wizz(int input, string expected)
        {
            // arrange
            string result = null;
            this.mockcheckday.Setup(m => m.SpecifiedDayChecK(It.IsAny<DayOfWeek>())).Returns(true);

            // act
            if (divisiblebythree.IsDivisible(input))
            {
                result = divisiblebythree.GetMessage();
            }

            // assert
            Assert.AreEqual(expected, result);
        }
    }
}