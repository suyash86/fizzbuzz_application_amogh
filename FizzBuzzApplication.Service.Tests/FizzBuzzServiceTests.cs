﻿namespace FizzBuzzApplication.Service.Tests
{
    using System.Collections.Generic;
    using FizzBuzzApplication.Service.Helper;
    using FizzBuzzApplication.Service.Interface;
    using FizzBuzzApplication.Service.Services;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class FizzBuzzServiceTests
    {
        private Mock<IDivisionByNumber> mockdivisionbythree;
        private Mock<IDivisionByNumber> mockdivisionbyfive;
        private IEnumerable<IDivisionByNumber> divisionbynumbers;
        private IFizzBuzzService fizzbuzzservice;
        private int input;
        private List<string> testlist;

        [SetUp]
        public void Initialize()
        {
            mockdivisionbythree = new Mock<IDivisionByNumber>();
            mockdivisionbyfive = new Mock<IDivisionByNumber>();
            divisionbynumbers = new List<IDivisionByNumber>() { mockdivisionbythree.Object, mockdivisionbyfive.Object };
            fizzbuzzservice = new FizzBuzzService(divisionbynumbers);
            this.mockdivisionbythree.Setup(m => m.IsDivisible(It.Is<int>(x => x % 3 == 0))).Returns(true);
            this.mockdivisionbythree.Setup(m => m.IsDivisible(It.Is<int>(x => x % 3 != 0))).Returns(false);
            this.mockdivisionbyfive.Setup(m => m.IsDivisible(It.Is<int>(x => x % 5 == 0))).Returns(true);
            this.mockdivisionbyfive.Setup(m => m.IsDivisible(It.Is<int>(x => x % 5 != 0))).Returns(false);
            input = 15;
        }

        [Test]
        public void GetFizzBuzzList_Test_ForNotSpecifiedDay()
        {
            // arange
            testlist = new List<string>() { "1", "2", "fizz", "4", "buzz", "fizz", "7", "8", "fizz", "buzz", "11", "fizz", "13", "14", "fizz buzz" };
            this.mockdivisionbythree.Setup(m => m.GetMessage()).Returns(FizzBuzzConstants.Fizz);
            this.mockdivisionbyfive.Setup(m => m.GetMessage()).Returns(FizzBuzzConstants.Buzz);

            // act
            var result = this.fizzbuzzservice.GetFizzBuzzList(this.input);

            // assert
            Assert.AreEqual(this.testlist, result);
        }

        [Test]
        public void GetFizzBuzzList_Test_ForSpecifiedDay()
        {
            // arrange
            testlist = new List<string>() { "1", "2", "wizz", "4", "wuzz", "wizz", "7", "8", "wizz", "wuzz", "11", "wizz", "13", "14", "wizz wuzz" };
            this.mockdivisionbythree.Setup(m => m.GetMessage()).Returns(FizzBuzzConstants.Wizz);
            this.mockdivisionbyfive.Setup(m => m.GetMessage()).Returns(FizzBuzzConstants.Wuzz);

            // act
            var result = fizzbuzzservice.GetFizzBuzzList(15);

            // assert
            Assert.AreEqual(testlist, result);
        }
    }
}