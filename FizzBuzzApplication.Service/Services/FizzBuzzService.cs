﻿namespace FizzBuzzApplication.Service.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using FizzBuzzApplication.Service.Interface;

    public class FizzBuzzService : IFizzBuzzService
    {
        private readonly List<string> fizzbuzzlist;
        private readonly IEnumerable<IDivisionByNumber> divisionbynumber;

        public FizzBuzzService(IEnumerable<IDivisionByNumber> divisionbynumber)
        {
            this.fizzbuzzlist = new List<string>();
            this.divisionbynumber = divisionbynumber;
        }

        public IEnumerable<string> GetFizzBuzzList(int input)
        {
            for (int index = 1; index <= input; index++)
            {
                var isDivisible = this.divisionbynumber.Where(m => m.IsDivisible(index)).ToList();
                fizzbuzzlist.Add(isDivisible.Any() ? string.Join(" ", isDivisible.Select(m => m.GetMessage())) : index.ToString());
            }

            return fizzbuzzlist;
        }
    }
}