﻿namespace FizzBuzzApplication.Service.Services
{
    using System;
    using FizzBuzzApplication.Service.Helper;
    using FizzBuzzApplication.Service.Interface;

    public class DivisibilityByThree : IDivisionByNumber
    {
        private readonly ICheckDay checkday;
        private readonly DayOfWeek today;

        public DivisibilityByThree(ICheckDay checkday)
        {
            this.checkday = checkday;
            this.today = DateTime.Now.DayOfWeek;
        }

        public bool IsDivisible(int input)
        {
            return input % 3 == 0;
        }

        public string GetMessage()
        {
            return checkday.SpecifiedDayChecK(today) ? FizzBuzzConstants.Wizz : FizzBuzzConstants.Fizz;
        }
    }
}