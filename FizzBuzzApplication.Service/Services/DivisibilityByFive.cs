﻿namespace FizzBuzzApplication.Service.Services
{
    using System;
    using FizzBuzzApplication.Service.Helper;
    using FizzBuzzApplication.Service.Interface;

    public class DivisibilityByFive : IDivisionByNumber
    {
        private readonly ICheckDay checkday;
        private readonly DayOfWeek today;

        public DivisibilityByFive(ICheckDay checkday)
        {
            this.checkday = checkday;
            this.today = DateTime.Now.DayOfWeek;
        }

        public bool IsDivisible(int input)
        {
            return input % 5 == 0;
        }

        public string GetMessage()
        {
            return checkday.SpecifiedDayChecK(today) ? FizzBuzzConstants.Wuzz : FizzBuzzConstants.Buzz;
        }
    }
}