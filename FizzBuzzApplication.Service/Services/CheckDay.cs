﻿namespace FizzBuzzApplication.Service.Services
{
    using System;
    using FizzBuzzApplication.Service.Interface;

    public class CheckDay : ICheckDay
    {
        private readonly string specifiedday;

        public CheckDay(string specifiedDay)
        {
            this.specifiedday = specifiedDay;
        }

        public bool SpecifiedDayChecK(DayOfWeek today)
        {
            return today.ToString() == specifiedday;
        }
    }
}