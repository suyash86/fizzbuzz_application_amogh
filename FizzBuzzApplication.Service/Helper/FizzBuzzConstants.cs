﻿namespace FizzBuzzApplication.Service.Helper
{
    public static class FizzBuzzConstants
    {
        public const string Fizz = "fizz";
        public const string Buzz = "buzz";
        public const string Wizz = "wizz";
        public const string Wuzz = "wuzz";
    }
}
