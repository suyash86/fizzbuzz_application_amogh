﻿namespace FizzBuzzApplication.Service.Interface
{
    using System;

    public interface ICheckDay
    {
        bool SpecifiedDayChecK(DayOfWeek today);
    }
}